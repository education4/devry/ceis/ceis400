There is no content here in the Wiki yet, but there is a great getting started article by [Tim Heuer](http://timheuer.com/blog/) here, which explains how to begin creating Windows Store apps with Sqlite and Sqlite-net:

* [HOWTO: SQLite with Windows 8 apps](http://timheuer.com/blog/archive/2012/08/07/updated-how-to-using-sqlite-from-windows-store-apps.aspx)
* [HOWTO: SQLite with Windows Phone apps](http://blogs.windows.com/windows_phone/b/wpdev/archive/2013/03/12/using-the-sqlite-database-engine-with-windows-phone-8-apps.aspx)

## Using SQLite in Windows Store Apps

[Robert Green](http://blogs.msdn.com/b/robertgreen/) shows how to add [SQLite](http://www.sqlite.org/about.html) support to your Windows Store apps. SQLite is a free open source library that provides a self-contained transactional SQL database engine. Watch as Robert demonstrates how to setup a project to use SQLite as well as how to query, add, update,  and delete items in the database. See the video [here](http://channel9.msdn.com/Shows/Visual-Studio-Toolbox/Using-SQLite-in-Windows-Store-Apps).

He writes in his blog on how to use [SQLite in Windows Store Apps](http://blogs.msdn.com/b/robertgreen/archive/2012/11/13/using-sqlite-in-windows-store-apps.aspx).  The same database code which Robert has detailed in this blog post can also be used for Windows Phone apps.