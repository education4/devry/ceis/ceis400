# Home

Welcome to [sqlite-net](https://github.com/praeclarum/sqlite-net), the simple and fast ORM and sqlite driver.

* [Getting Started](Getting-Started.md)
* [Using the Synchronous API](Synchronous-API.md)
* [Using the Async API](Asynchronous-API.md)
* [FAQ](FAQ.md)
* [How to Contribute](How-to-Contribute.md)
* [Using sqlite-net with Metro-style](Windows-Store-and-Windows-Phone-apps.md)
* [Features](Features.md)
  - [Transactions](Transactions.md)
  - [Automatic Migrations](Automatic-Migrations.md)
* [Performance](Performance.md)
* [Extensions](sqlite-net-extensions(deprecated).md)