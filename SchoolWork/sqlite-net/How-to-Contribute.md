# How to Contribute

We want contributions. We need contributions. You should contribute!

If you find a bug, then follow these easy steps:

1. Create an Issue for the bug tagged "Bug".
2. Write a regression test in our awesome Test library under one of the appropriate fixtures.
3. Fix it!
4. Commit & Push & Submit a Pull Request telling me how awesome your fix is. Make sure to include "Fix #XXX" in the commit message.

If you want a new feature, then follow these steps:

1. Implement it!
2. Test it a bunch. If it's a small feature then add some tests to an appropriate fixture. If it's big, you better create a new test fixture.
3. Commit & Push & Submit a Pull Request telling me how awesome and valuable this new feature is.

## Submitting Patches (Pull Requests)

1. Fork the repository
2. Check out the repository onto your machine
3. Hack on src/SQLite.cs (but don't change the formatting! If you use VS's auto format the gods will frown upon you)
4. Commit & push your changes
5. Open a pull request on praeclarum/sqlite-net

See [the GitHub docs on how to fork a repo](https://help.github.com/articles/fork-a-repo) for more details.

## Code Formatting

* Use Tabs aligned to 4 characters
* All Members are PascalCase except for private fields are camelCase with a leading _
* Curly braces go on the same line of the statement needing them except Namespaces, Classes, and Method definitions get them on the next line.

