# Frequently Asked Questions

## With MonoTouch, I get the error: "Method not found"

Make sure you're not "linking" your code. Linking removes code that you don't call directly. Either switch the project to "Link SDKs only" or use the [Preserve] attribute.

Because sqlite-net uses reflection, the MT linker doesn't recognize that it needs various properties. It then strips those properties off of the class.
