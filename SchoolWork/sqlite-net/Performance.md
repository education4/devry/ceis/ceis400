# Performance

## Serializing 5000 objects on iOS 5 with an iPhone 4S

The test was performed using the following code: https://gist.github.com/3137502

### Serialization
<table>
<tr><th>Serializer</th><th>Time (s)</th></tr>
<tr><td>BinaryFormatter</td><td>0.6671089</td></tr>
<tr><td>BinaryWriter Explicit</td><td>0.1163388</td></tr>
<tr><td>BinaryWriter Reflection</td><td>0.4785611</td></tr>
<tr><td><b>sqlite-net</b></td><td><b>0.7273159</b></td></tr>
</table>

### Deserialization
<table>
<tr><th>Serializer</th><th>Time (s)</th></tr>
<tr><td>BinaryFormatter</td><td>1.3048520</td></tr>
<tr><td>BinaryWriter Explicit</td><td>0.1911826</td></tr>
<tr><td>BinaryWriter Reflection</td><td>0.6832386</td></tr>
<tr><td><b>sqlite-net</b></td><td><b>0.7749375</b></td></tr>
</table>

The following object was used in these tests:

	[Serializable]
	class TestObject {		
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime ModifiedTime { get; set; }
		public double Value { get; set; }
		public string Text { get; set; }
		public int Last { get; set; }
	}
